package com.boeing.steps.proto.react01;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

@Component
public class DatabaseLoader implements CommandLineRunner {
	
	private static final Logger log = LoggerFactory.getLogger(DatabaseLoader.class);

	private final EmployeeRepository repository;

	@Autowired
	public DatabaseLoader(EmployeeRepository repository) {
		log.info("Constructor");
		this.repository = repository;
	}

	@Override
	public void run(String... strings) throws Exception {
		log.info("run");
		this.repository.save(new Employee("Frodo", "Baggins", "ring bearer"));
	}
}