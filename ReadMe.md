# React Proto

Modeled off of:

`https://spring.io/guides/tutorials/react-and-spring-data-rest/`  
and  
`https://github.com/spring-guides/tut-react-and-spring-data-rest/`

# Steps to building

1. Create Gradle Project in Eclipse
    * Create a `.gitignore` file
    * Create a `ReadMe.md` file
1. Add the proper dependencies
    * Not as straight forward as you might think;
    * Choose spring-boot-starter-thymeleaf,  spring-boot-starter-data-jpa and spring-data-rest-webmvc
    * Added spring-data-rest-webmvc becuase this allows the use of `RepositoryRestConfigurerAdapter`
    * Added HSDB so there is an available auto-configure data source 
    * Included lombok as it was in the demo and adds getter/setter methods 
1. npm init
    * Answer the questions, lots of defaults: creates a skeleton `package.json` file
    * `package.json`  lists the Javascript dependencies for the project (none initially)
    * Added dependencies via `npm install react  react-dom rest webpack --save` 
    * (the `--save` updates the `package.json` file.)
    *  `npm install --save-dev babel-loader babel-core`
1. Added the Spring Java code
    * Created classes in `com.boeing.steps.proto.react01`
    * added `CustomizedRestConfiguration` to set the rest API end point
    * adding the file `application.properties` as per the guide did not work, but the configuration is java was prefered anyway.
    * added some logging to see what was running
1. Added js files
    * `src/main/js/app.js` and `src/main/js/client.js`
    * truncated `src/main/js/client.js` because it was causing errors (it's still truncated)
1. Added static files (file) in `src/main/resources`
    * Added `index.html`
1. Per the guide I `webpack`ed the code
    * `node_modules\.bin\webpack -d --config webpack.config.js`
    * Which creates a bundle in `src/main/resources`
1. `gradlew.bat bootRun`
    * Rock and Roll, baby!
    * Browse the api `http://localhost:8080/api`
    * Browse the application `http://localhost:8080` (which does not work because `client.js` is truncated.)
    
# Some things I ran into, learned, or am guessing about

The bundle.js file is built with `webpack` and the `babel` javascript processor (compiler). 
The guide used the earlier version of babel which failed. I switched to the newer version, 
`babel-core` and `babel-loader`. This then mostly worked but failed on some part of 
the guide's `client.js`. I truncated `client.js` to get it two pass the packing stage.

A build process will have to include a step to "build" the UX. This is not a native task
in gradle, not without a plug-in of some sort. At the momement I don't trust any of the 
searchable such plug-ins. So by hand I did the webpack (seen above) but this should be 
in the build script.

I'm wondering if I can avoid packing and just do copying? I'd prefer on my PC and in
dev environments to have debug the code as it's written, not as it's packed. Not sure if
this is a choice in webpack or if it requires a speical webpack loader. Or if I ditch
webpack all together and just use native js. Not sure of the performance implications
for apps that are not world-deployed (just enterprise deployed).











    
