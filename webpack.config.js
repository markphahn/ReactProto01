var path = require('path');

module.exports = {
    entry: './src/main/js/app.js',
    devtool: 'sourcemaps',
    cache: true,
    //debug: true,
    output: {
        path: __dirname,
        filename: './src/main/resources/static/built/bundle.js'
    },
    module: {
        rules: [
        { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" }
      ]
    }

//        loaders: [
//            {
//                test: path.join(__dirname, '.'),
//                exclude: /(node_modules)/,
//                loader: 'babel-core',
//                query: {
//                    cacheDirectory: true,
//                    presets: ['es2015', 'react']
//                }
//            }
//        ]
//    }
};
